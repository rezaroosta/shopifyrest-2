﻿using System.Collections.Generic;
using System.Linq;
using ShopifyRest.Objects.Common;
using ShopifyRest.Objects.Filters;
using ShopifyRest.Objects.Filters.Enums.FilterFields;
using ShopifyRest.Objects.Orders.Tracking;
using ShopifyRest.Objects.Products;
using ShopifyRest.Objects.Webhooks;
using ShopifyRest.Services.Implementation;
using ShopifyRest.Utils;
using ShopifyWebhookTopic = ShopifyRest.Objects.Webhooks.ShopifyWebhookTopic;

namespace ShopifyTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var x = new ShopifyProductService(new ShopifySettings("7148fd9edfd9f9557ff4e20a0ef0be80", "b42bc6c4d02d11d4de5ef543d16625fc", "vanstore.myshopify.com"));

            x.GetAll();

            //var y = new ShopifyProductService(new ShopifySettings()
            //{
            //    ApiKey = "49fc7a7e93fbf13f53128977c5b7f246",
            //    Password = "5707c9bdb390b3127fc06a6af9442bb9",
            //    HostName = "ak-store-4.myshopify.com"
            //});
            //
            //y.Create(new ShopifyProduct());

            //var z = new ShopifyCustomerService(new ShopifySettings()
            //{
            //    ApiKey = "49fc7a7e93fbf13f53128977c5b7f246",
            //    Password = "5707c9bdb390b3127fc06a6af9442bb9",
            //    HostName = "ak-store-4.myshopify.com"
            //});

            //var k = new ShopifyAssetService(new ShopifySettings()
            //{
            //    ApiKey = "49fc7a7e93fbf13f53128977c5b7f246",
            //    Password = "5707c9bdb390b3127fc06a6af9442bb9",
            //    HostName = "ak-store-4.myshopify.com"
            //});

            //var l = new ShopifyPageService(new ShopifySettings()
            //{
            //    ApiKey = "49fc7a7e93fbf13f53128977c5b7f246",
            //    Password = "5707c9bdb390b3127fc06a6af9442bb9",
            //    HostName = "ak-store-4.myshopify.com"
            //});



            //var rr = l.GetAll();

            //l.GetAll();

            //var id = z.GetAll().First().Id;
            //var themeID = 122871811;

            //var ass = k.GetAll(themeID);

            // z.ActivationUrl(id.ToString());

            // var cs = z.Search(new ShopiyCustomerSearchRequest()
            // {
            //     Query = "stas"
            // });

            // var ress = y.Cancel(4035624899, new ShopifyOrderCancelOptions()
            // {
            //     Reason = "some reason",
            //     Refund = true,
            //     RefundAmount = 20.2
            // });



            //TestProductVariant();
            //var os = y.GetAll();

            //var res = x.Filter<ShopifyProduct>(new ShopifyProductFilterRequest()
            //{
            //    Fields = new List<string>() { "title", "id" },
            //    ProductType = "fdfd",
            //    PublishedStatus = ShopifyPublishedStatus.Unpublished,
            //    IDs = new List<long>() { 7885780163 }
            //});

            //var v = x.GetByID(8706713155);
            //var v2 = x.GetByIDs(new List<long>() { 8706713155 });

            //var t1 = x.CreateProduct(new ShopifyProduct()
            //{

            //    Body = v.Body,
            //    ProductType = v.ProductType,
            //    Tags = v.Tags,
            //    Title = v.Title,
            //    Vendor = v.Vendor,
            //    Variants = null,
            //    Images = v.Images
            //});



            // v.Id = 0;
            // var t4 = x.CreateProduct(v);

            // v.Title = v.Title + "new Test"; 

            //var t2 = x.UpdateProduct(v);

            //var t3 = x.GetAll();

            // x.DeleteProduct(8706713155);


            //TestTran();
            //   var settings = new ShopifySettings()
            //   {
            //       ApiKey = "49fc7a7e93fbf13f53128977c5b7f246",
            //       Password = "5707c9bdb390b3127fc06a6af9442bb9",
            //       HostName = "ak-store-4.myshopify.com"
            //   };
            //
            //   var res = Net.MakeGet("ak-store-4.myshopify.com/dsfsf/sdfdsf.json", settings);
        }

        private static void TestWebhook()
        {
            //webhookID = 468077646
            var service = new ShopifyWebhookService(new ShopifySettings("ak-store-4.myshopify.com", "49fc7a7e93fbf13f53128977c5b7f246", "5707c9bdb390b3127fc06a6af9442bb9"));

            var create = service.Create(new ShopifyWebhook()
            {
                Topic = ShopifyWebhookTopic.CartCreated,
                Address = "https://ak-store-4.myshopify.com11",
                Format = "json"
            });
            //DONE
            // var create = service.Update(new ShopifyWebhook()
            // {
            //     Topic = ShopifyWebhookTopic.CartCreated,
            //     Address = "https://ak-store-4.myshopify.com111",
            //     Format = "json",
            //     Id = 468077646
            // });
            //DONE
            // var getByID = service.GetByID(468077646);
            var d = 1;
        }

        private static void TestProductVariant()
        {
            //productID = 8706713155
            //variantID = {31661589966,31661622990,31661931598}
            var service = new ShopifyVariantService(new ShopifySettings("ak-store-4.myshopify.com", "49fc7a7e93fbf13f53128977c5b7f246", "5707c9bdb390b3127fc06a6af9442bb9"));

            //TEST DONE
            /* var create = service.Create(new ShopifyVariant()
             {
                 Option = "sdf11231",
                 Price =1,

             }, 8706713155);*/

            //TEST DONE
            /*var update = service.Update(new ShopifyVariant()
            {
                Option = "sdf1123",
                Price = 1,
                Id = 31661622990,
                ProductId = 8706713155

            });*/

            //TEST DONE
            var getByID = service.GetByID(31661622990);

            var getAll = service.GetAll(8706713155);

            var filter = service.Filter(new ShopifyProductVariantFilterRequest()
            {
                Fields = new List<ShopifyProductVariantField>()
                {
                    ShopifyProductVariantField.FulfillmentService, ShopifyProductVariantField.Grams
                }
            }, 8706713155);

            var d = "dfsf";
        }

        private static void TestProductImage()
        {
            //productID = 8706713155
            //imageID = {18819060558,18819063118}, 

            var service = new ShopifyProductImageService(new ShopifySettings("ak-store-4.myshopify.com", "49fc7a7e93fbf13f53128977c5b7f246", "5707c9bdb390b3127fc06a6af9442bb9"));

            //Test DONE
            /*var create = service.Create(new ShopifyImage("http://www.planwallpaper.com/static/images/desktop-year-of-the-tiger-images-wallpaper.jpg")
            {
                ProductId = 8706713155,
                Src = "http://www.planwallpaper.com/static/images/desktop-year-of-the-tiger-images-wallpaper.jpg"
            }, 8706713155);*/

            //TEST DONE
            /*var update = service.Create(new ShopifyImage("http://assets.barcroftmedia.com.s3-website-eu-west-1.amazonaws.com/assets/images/recent-images-11.jpg")
            {
                ID= 18819063118,
                ProductId = 8706713155,
                Src = "http://assets.barcroftmedia.com.s3-website-eu-west-1.amazonaws.com/assets/images/recent-images-11.jpg"
            }, 8706713155); */

            //TEST Done
            //  var getByID = service.GetByID(18819060558, 8706713155);

            //TEST DONE
            //   var getALl = service.GetAll(8706713155);

            //TEST DONE
            /* var filter = service.Filter(new ShopifyProductImageFilterRequest()
             {

             }, 8706713155);*/

            var d = "d";
        }

        private static void TestPage()
        {
            //PageIDs = 213910030, 213910094,
            var service = new ShopifyPageService(new ShopifySettings("ak-store-4.myshopify.com", "49fc7a7e93fbf13f53128977c5b7f246", "5707c9bdb390b3127fc06a6af9442bb9"));

            //TEST Done
            /* var create = service.Create(new ShopifyPage()
             {
                 Title = "Test1",
                 BodyHtml = "<h1>Warranty<\\ /h1>\n<p><strong>Forget it<\\/strong>, we aint giving you nothing<\\/p>",

             });*/

            //TEST Done
            /* var update = service.Update(new ShopifyPage()
             {
                 Title = "Test1123",
                 BodyHtml = "<h1>Warranty<\\ /h1>\n<p><strong>Forget it<\\/strong>, we aint giving you nothing<\\/p>",
             }, 213910030);*/

            /*TEST DONE*/
            //var getByID = service.GetByID(213910030);

            //TEST FAILED
            //var GetByIDs = service.GetByIDs(new List<long>() {213910030, 213910094});

            /*TEST DONE*/
            var getALl = service.GetAll();

            /*TEST DONE
            /*var filter = service.Filter(new ShopifyPageFilterRequest()
            {
                Fields = new List<ShopifyPageField>()
                {
                    ShopifyPageField.BodyHtml, ShopifyPageField.CreatedAt, ShopifyPageField.Title
                }
            });*/


            var breakasd = true;
        }

        private static void TestAddress()
        {
            //customerID = 4883330254,4883340942
            //addressID =  {4998743310,4998794766},4998751118
            var service = new ShopifyAddressService(new ShopifySettings("ak-store-4.myshopify.com", "49fc7a7e93fbf13f53128977c5b7f246", "5707c9bdb390b3127fc06a6af9442bb9"));

            //TEST DONE
            // var getByID = service.GetByID(4998743310, 4883330254);

            //TEST DONE
            //  var getAll = service.GetAll(4883330254);

            //addressID = 4998794766
            /*TEST DONE*/
            /*var create = service.Create(new ShopifyAddress()
            {
                Address1 = "123 Oak St123",
                Address2 = "",
                City = "Ottawa",
                Province = "ON",
                Phone = "555-1212",
                Zip = "123 ABC",
                LastName = "Lastnameson",
                FirstName = "Mother",
                Country = "CA"
            }, 4883330254);*/

            //TEST DONe
            /*   var update = service.Update(new ShopifyAddress()
               {
                   Address1 = "123 Oak St1231234123",
                   Address2 = "",
                   City = "Ottawa",
                   Province = "ON",
                   Phone = "555-1212",
                   Zip = "123 ABC",
                   LastName = "Lastnameson",
                   FirstName = "Mother",
                   Country = "CA",
                   Id = 4998794766
               }, 4883330254);*/

            //var breakpoint = true;
        }

        private static void TestCustomer()
        {
            var service = new ShopifyCustomerService(new ShopifySettings("ak-store-4.myshopify.com", "49fc7a7e93fbf13f53128977c5b7f246", "5707c9bdb390b3127fc06a6af9442bb9"));

            //customerID = 4883330254,4883340942,4883355406
            //addressID =  4998743310,4998751118

            //TEST DONE
            /*   var create = service.Create(new ShopifyCustomer()
                {
                    FirstName = "Stev75",
                    LastName = "Lastnameson",
                    Email = "steve.1231111lastnameson@example.com",
                    VerifiedEmail = true,
                    Addresses = new List<ShopifyAddress>()
                    {
                       new ShopifyAddress()
                       {
                           Address1 = "123 Oak St",
                           Address2 = "",
                           City = "Ottawa",
                           Province = "ON",
                           Phone = "555-1212",
                           Zip = "123 ABC",
                           LastName = "Lastnameson",
                           FirstName = "Mother",
                           Country = "CA"
                       } 
                    }

                });*/

            //TEST DONE
            /*var update = service.Update(new ShopifyCustomer()
            {
                Id= 4883330254,
                FirstName = "Steve2",
                LastName = "Lastnameson",
                Email = "steve.123lastnameson@example.com",
                VerifiedEmail = true,
                Addresses = new List<ShopifyAddress>()
                {
                   new ShopifyAddress()
                   {
                       Address1 = "123 Oak St",
                       Address2 = "",
                       City = "Ottawa",
                       Province = "ON",
                       Phone = "555-1212",
                       Zip = "123 ABC",
                       LastName = "Lastnameson",
                       FirstName = "Mother",
                       Country = "CA"
                   }
                }

            });*/

            //TEST DONE
            //  var getByID = service.GetByID(4883330254);

            //TEST Done
            // var getByIDs = service.GetByIDs(new List<long>() {4883330254, 4883340942});

            //TEST DONE
            //  var getALL = service.GetAll();

            //TEST DONE
            /*  var filter = service.Filter(new ShopifyCustomerFilterRequest()
              {
                  IDs = new List<long>()
                  {
                      4883330254, 4883340942
                  },
                 // CreatedAtMax = 4883330254, 4883340942}
                 Fields = new List<ShopifyCustomerField>()
                 {
                     ShopifyCustomerField.CreatedAt
                 }
              });*/

            //TEST DONE
            /* service.Delete(4883355406);
             var getByID = service.GetByID(4883355406);*/

            //   var breakpoint = true;
        }

        private static void TestAssets()
        {
            var settings = new ShopifySettings("ak-store-4.myshopify.com", "49fc7a7e93fbf13f53128977c5b7f246", "5707c9bdb390b3127fc06a6af9442bb9");

            var themeService = new ShopifyThemeService(settings);

            var themeId = themeService.GetAll().First().Id;

            var assetService = new ShopifyAssetService(settings);

            //var allAssets = assetService.GetByTheme(themeId);



        }

        private static void TestTheme()
        {
            var settings = new ShopifySettings("ak-store-4.myshopify.com", "49fc7a7e93fbf13f53128977c5b7f246", "5707c9bdb390b3127fc06a6af9442bb9");

            var themeService = new ShopifyThemeService(settings);

            var themeId = themeService.GetAll().First().Id;

            //var getById = themeService.GetById(themeId);

            //var create = themeService.Create(new ShopifyTheme()
            //{
            //    Name = "dsgfds",
            //    Role = "unpublished"
            //});


        }

        private static void FullFilment()
        {
            var settings = new ShopifySettings("ak-store-4.myshopify.com", "49fc7a7e93fbf13f53128977c5b7f246", "5707c9bdb390b3127fc06a6af9442bb9");

            var fullfilmentSrvice = new ShopifyFulfillmentService(settings);

            var orderService = new ShopifyOrderService(settings);

            var orderId = orderService.GetAll().First().Id;

            var all = fullfilmentSrvice.GetAll(orderId);

            var create = fullfilmentSrvice.Create(new ShopifyFulfillment()
            {
                TrackingNumber = "dsfdsf",
                TrackingCompany = "Fex ex"
            }, orderId);

            var getbyid = fullfilmentSrvice.GetByID(create.Id, orderId);
        }

        public static void TestOrder() //DONE
        {
            var settings = new ShopifySettings("ak-store-4.myshopify.com", "49fc7a7e93fbf13f53128977c5b7f246", "5707c9bdb390b3127fc06a6af9442bb9");

            var orderService = new ShopifyOrderService(settings);

            var all = orderService.GetAll();

            var getbyid = orderService.GetByID(all.First().Id);

            //var create = orderService.Create(new ShopifyOrder()
            //{
            //    Email = "gfdf@gfdsg.dfsg",
            //    FulfillmentStatus = ShopifyFulfillmentStatus.Fulfilled.ToString()
            //});
            var tempName = getbyid.Name;

            // getbyid.Name = "some name";
            // getbyid.Id = 0;
            //getbyid.SourceName = null;
            getbyid.Note = "lol";

            //var createOrder = orderService.Create(getbyid);

            //createOrder.Name = "tests2";


            var update = orderService.Update(getbyid);

            //var update = orderService.Update(getbyid);

            //update.Name = tempName;

            //var update2 = orderService.Update(getbyid);
        }

        public static void TestProduct()
        {
            var settings = new ShopifySettings("ak-store-4.myshopify.com", "49fc7a7e93fbf13f53128977c5b7f246", "5707c9bdb390b3127fc06a6af9442bb9");

            var productService = new ShopifyProductService(settings);

            var all = productService.GetAll();

            var getById = productService.GetByID(all.First().Id);

            //getById.Id = 0;
            //getById.Images[0].Id = 0;
            //getById.Images[0].ProductId = 0;

            //var create = productService.Create(getById);
            getById.Body = "test";



            var update = productService.Update(getById);
        }

        public static void TestTran()
        {
            var settings = new ShopifySettings("ak-store-4.myshopify.com", "49fc7a7e93fbf13f53128977c5b7f246", "5707c9bdb390b3127fc06a6af9442bb9");

            var tranService = new ShopifyTransactionService(settings);
            var orderService = new ShopifyOrderService(settings);

            var orderID = orderService.GetAll().First().Id;

            var all = tranService.GetAll(orderID);

            var getbyid = tranService.GetByID(all.First().Id, orderID);

        }

    }
}